# 64 bit
JDK=jdk-8u91-linux-x64.tar.gz
CODE_DIR=$SCRIPTS_DIR/../code
JAVA_DIR=$SCRIPTS_DIR/../../java
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MAVEN_DIR=$SCRIPTS_DIR/../../maven
HOME_DIR=$(eval echo ~${SUDO_USER})


#Installing Java
echo "Installing Java"
$SCRIPTS_DIR/install-java.sh
echo "Java Installed"

#Installing Maven
echo "Installing Maven"
$SCRIPTS_DIR/install-maven.sh
echo "Maven Installed"


#Installing Tomcat
echo "Installing Tomcat"
$SCRIPTS_DIR/install-tomcat.sh
echo "Tomcat Installed"

#Starting Tomcat
echo "Starting Tomcat"
~/tomcat/bin/catalina.sh start

#create appropriate folders
cd ~
mkdir apps backups logs uploads bin temp

