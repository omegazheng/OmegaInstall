# deploy code
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir /home/omega/apps/OmegaCms

cp /home/omega/OmegaInstall/install/apps/j.xml /home/omega/tomcat/conf/Catalina/localhost/

mkdir /home/omega/apps/conf/
# cp /home/omega/OmegaInstall/install/apps/conf/gelfHttpCms.conf /home/omega/apps/conf/
cp /home/omega/OmegaInstall/install/apps/conf/OmegaCms.conf /home/omega/apps/conf/
cp /home/omega/OmegaInstall/install/apps/conf/quartz.properties /home/omega/apps/conf/
# cp deploy script
cp /home/omega/OmegaInstall/install/apps/bin/deployCms.sh /home/omega/bin/
chmod +x /home/omega/bin/deployCms.sh
cp /home/omega/OmegaInstall/install/apps/bin/displayVersionCms.sh /home/omega/bin
chmod +x /home/omega/bin/displayVersionCms.sh
cp /home/omega/OmegaInstall/install/code/index_cms.jsp /home/omega/tomcat/webapps/ROOT/index.jsp

#deployCms.sh
echo -n "Enter your war file name and press [ENTER]: "
read filename

sh /home/omega/bin/deployCms.sh /home/omega/uploads/$filename
