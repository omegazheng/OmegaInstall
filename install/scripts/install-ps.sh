# deploy code
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir /home/omega/apps/OmegaPs
cd /home/omega/apps/OmegaPs

cp /home/omega/OmegaInstall/install/apps/ps.xml /home/omega/tomcat/conf/Catalina/localhost

mkdir /home/omega/apps/conf/
# cp /home/omega/OmegaInstall/install/apps/conf/gelfHttpPs.conf /home/omega/apps/conf/
cp /home/omega/OmegaInstall/install/apps/conf/OmegaPs.conf /home/omega/apps/conf/

# cp deploy script
cp /home/omega/OmegaInstall/install/apps/bin/deployPs.sh /home/omega/bin
chmod +x /home/omega/bin/deployPs.sh
cp /home/omega/OmegaInstall/install/apps/bin/displayVersionPs.sh /home/omega/bin
chmod +x /home/omega/bin/displayVersionPs.sh

#deployPs.sh
echo -n "Enter your war file name and press [ENTER]: "
read filename

deployPs.sh /home/omega/uploads/$filename
