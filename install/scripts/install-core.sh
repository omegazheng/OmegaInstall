# deploy code
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir /home/omega/apps/OmegaCore

cp /home/omega/OmegaInstall/install/apps/core.xml /home/omega/tomcat/conf/Catalina/localhost/

# cp deploy script
cp /home/omega/OmegaInstall/install/apps/bin/deployCore.sh /home/omega/bin/
chmod +x /home/omega/bin/deployCore.sh
cp /home/omega/OmegaInstall/install/code/index_core.jsp /home/omega/tomcat/webapps/ROOT/index.jsp

#deployCore.sh

sh /home/omega/bin/deployCore.sh
