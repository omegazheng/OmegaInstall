SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# deploy code
mkdir /home/omega/apps/OmegaIcs
cd /home/omega/apps/OmegaIcs

cp /home/omega/OmegaInstall/install/apps/ics.xml /home/omega/tomcat/conf/Catalina/localhost

mkdir /home/omega/apps/conf/
cp /home/omega/OmegaInstall/install/apps/conf/OmegaIcs.conf /home/omega/apps/conf/

# cp logback file
cp /home/omega/OmegaInstall/install/apps/conf/ics_logback.properties /home/omega/apps/conf/

# cp deploy script
cp /home/omega/OmegaInstall/install/apps/bin/deployIcs.sh /home/omega/bin
chmod +x /home/omega/bin/deployIcs.sh
cp /home/omega/OmegaInstall/install/apps/bin/displayVersionIcs.sh /home/omega/bin
chmod +x /home/omega/bin/displayVersionIcs.sh



deployIcs.sh
