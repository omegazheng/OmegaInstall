#64 bit
JDK=jdk-8u181-linux-x64.tar.gz
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CODE_DIR=$SCRIPTS_DIR/../code
JAVA_DIR=$SCRIPTS_DIR/../../../java

#Download JDK if requried
$SCRIPTS_DIR/download-java.sh

#Unzip the jdk to the right folder
cd $CODE_DIR
tar -zxf $JDK
cp -rf $CODE_DIR/jdk1.8.0_181/ $JAVA_DIR


#setup bash_profile
sudo rm ~/.bash_profile
echo 'if [ -f ~/.bashrc ];' >> ~/.bash_profile && echo 'then . ~/.bashrc  ' >> ~/.bash_profile && echo 'fi' >> ~/.bash_profile && echo 'JAVA_HOME=/home/omega/java' >> ~/.bash_profile && echo 'PATH=$PATH:$HOME/.local/bin:$HOME/bin:$JAVA_HOME/bin' >> ~/.bash_profile && echo 'export PATH'>> ~/.bash_profile && source ~/.bash_profile
source ~/.bash_profile

echo java -version