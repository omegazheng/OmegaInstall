
cd ~

#Check out the hippo project
git clone https://github.com/omegasys/palace.git

#Override hippo host configuration for dev.
cp ~/palace/conf/omega_conf/hosts.xml ~/palace/bootstrap/configuration/src/main/resources/hst/

#Build app
cd ~/palace
mvn clean verify


echo -n "Enter your profile name and press [ENTER]: "
read profile

mvn -P $profile

cp ~/palace/target/palace-0.3.0-distribution.tar.gz ~/tomcat/
cd ~/tomcat/
tar zxvf palace-0.3.0-distribution.tar.gz


