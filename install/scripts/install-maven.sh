# 64 bit
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MAVEN_DIR=$SCRIPTS_DIR/../../maven
HOME_DIR=$(eval echo ~${SUDO_USER})

#Download MAVEN if requried
cd $HOME_DIR
wget http://mirror.csclub.uwaterloo.ca/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz

#Extract maven
tar -zxvf $HOME_DIR/apache-maven-3.3.9-bin.tar.gz -C $HOME_DIR

#move maven to a new folder
mv apache-maven-3.3.9 maven
rm apache-maven-3.3.9-bin.tar.gz

sudo rm /usr/local/maven
sudo ln -s $HOME_DIR/maven  /usr/local/maven

cd $HOME_DIR

#setup maven profile
sudo rm /etc/profile.d/maven.sh
echo 'export M2_HOME=/usr/local/maven' | sudo tee -a /etc/profile.d/maven.sh && echo 'export M2=$M2_HOME/bin' | sudo tee -a /etc/profile.d/maven.sh && echo 'PATH=$M2:$PATH ' | sudo tee -a /etc/profile.d/maven.sh

echo mvn -version
