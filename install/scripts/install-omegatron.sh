SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# deploy code
mkdir /home/omega/apps/OmegaTron
cd /home/omega/apps/OmegaTron

cp /home/omega/OmegaInstall/install/apps/omegatron.xml /home/omega/tomcat/conf/Catalina/localhost

mkdir /home/omega/apps/conf/
# cp /home/omega/OmegaInstall/install/apps/conf/gelfHttpTron.conf /home/omega/apps/conf/
cp /home/omega/OmegaInstall/install/apps/conf/OmegaTron.conf /home/omega/apps/conf/

# cp logback file
cp /home/omega/OmegaInstall/install/apps/conf/logback.properties /home/omega/apps/conf/

# cp deploy script
cp /home/omega/OmegaInstall/install/apps/bin/deployOmegaTron.sh /home/omega/bin
chmod +x /home/omega/bin/deployOmegaTron.sh
cp /home/omega/OmegaInstall/install/apps/bin/displayVersionTxs.sh /home/omega/bin
chmod +x /home/omega/bin/displayVerisonTxs.sh

#deployOmegaTron.sh
echo -n "Enter your war file name and press [ENTER]: "
read filename

deployOmegaTron.sh /home/omega/uploads/$filename
