JDK=jdk-8u181-linux-x64.tar.gz
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CODE_DIR=$SCRIPTS_DIR/../code
cd $CODE_DIR
echo "*******************************************"
echo "Downloading  $JDK to  $CODE_DIR"
echo "*******************************************"

wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/jdk-8u181-linux-x64.tar.gz

