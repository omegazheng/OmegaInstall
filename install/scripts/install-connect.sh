SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# deploy code
mkdir /home/omega/apps/OmegaConnect
cd /home/omega/apps/OmegaConnect

cp /home/omega/OmegaInstall/install/apps/connect.xml /home/omega/tomcat/conf/Catalina/localhost

mkdir /home/omega/apps/conf/
# cp /home/omega/OmegaInstall/install/apps/conf/gelfHttpConnect.conf /home/omega/apps/conf/
cp /home/omega/OmegaInstall/install/apps/conf/OmegaConnect.conf /home/omega/apps/conf/

# cp deploy script
cp /home/omega/OmegaInstall/install/apps/bin/deployConnect.sh /home/omega/bin
chmod +x /home/omega/bin/deployConnect.sh

#deployConnect.sh
echo -n "Enter your war file name and press [ENTER]: "
read filename

deployConnect.sh /home/omega/uploads/$filename
