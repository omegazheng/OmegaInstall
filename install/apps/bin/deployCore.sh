
echo "Backing up Core"
cp -rip /home/omega/apps/OmegaCore/ "/home/omega/backups/OmegaCore_`date +%Y_%m_%d_%H:%M`"

echo "remove old OmegaCore"
cd /home/omega/apps
rm -rf OmegaCore/
rm core.tar.gz

echo "deploy new OmegaCore"
cd /home/omega/uploads
cp core.tar.gz /home/omega/apps

cd /home/omega/apps
tar zxvf core.tar.gz

mv dist/ OmegaCore

echo "Done"

