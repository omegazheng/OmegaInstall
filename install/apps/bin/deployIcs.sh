
echo "Stopping tomcat"
/home/omega/tomcat/bin/catalina.sh stop 

echo "Backing up"
cp -r /home/omega/apps/OmegaIcs/ "/home/omega/backups/OmegaIcs_`date +%Y_%m_%d_%H:%M`"

echo "remove old OmegaIcs"
cd /home/omega/apps
rm -rf OmegaIcs/
rm ics.tar.gz

echo "deploy new OmegaIcs"
cd /home/omega/uploads
cp ics.tar.gz /home/omega/apps

cd /home/omega/apps
tar zxvf ics.tar.gz

mv OmegaIcsApps-4.00/ OmegaIcs

echo "Done"

echo "Starting up tomcat"
/home/omega/tomcat/bin/catalina.sh start


