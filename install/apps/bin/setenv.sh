# 1024
# 2048
# 4096
# 8192
# 10240
# 12288
# 16384

# Hippo Specific configs
# REPO_BOOT_OPTS="-Drepo.bootstrap=true"
# REPO_PATH="-Drepo.path=${CATALINA_BASE}/storage"
# REP_OPTS="-Drepo.config=file:${CATALINA_BASE}/conf/repository.xml"
# CATALINA_OPTS="${REPO_BOOT_OPTS} ${REPO_PATH} ${REP_OPTS}"

#!/bin/sh
# Grant Rosenthal May 2018
#
# ENVARS for Tomcat Omega environment - PS
# System version 4 beta
#
CATALINA_HOME="/home/omega/tomcat"
CATALINA_BASE="/home/omega/tomcat"
JAVA_HOME="/home/omega/java"
#
MAX_HEAP=4096
MIN_HEAP=2048
#
REPO_BOOT_OPTS="-Drepo.bootstrap=true"
REPO_PATH="-Drepo.path=${CATALINA_BASE}/storage"
REP_OPTS="-Drepo.config=file:${CATALINA_BASE}/conf/repository.xml"
# CATALINA_OPTS="${REPO_BOOT_OPTS} ${REPO_PATH} ${REP_OPTS}"
DMP_OPTS="-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/cms/heapdumps"
JMX_OPTS="-Dcom.sun.management.jmxremote=true \
                  -Dcom.sun.management.jmxremote.port=8086 \
                  -Dcom.sun.management.jmxremote.ssl=false \
                  -Dcom.sun.management.jmxremote.authenticate=false \
                  -Djavax.net.debug=all \
                  -Djava.rmi.server.hostname=10.0.1.145"

CATALINA_OPTS="${DMP_OPTS} ${JMX_OPTS} ${REPO_BOOT_OPTS} ${REPO_PATH} ${REP_OPTS}"
#
#
JVM_OPTS="-server -Xmx${MAX_HEAP}m -Xms${MIN_HEAP}m -XX:+UseG1GC -Djava.util.Arrays.useLegacyMergeSort=true -XX:+CMSClassUnloadingEnabled -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:omegaGC.log"

JAVA_OPTS="${JVM_OPTS} "
#
export JAVA_HOME CATALINA_HOME CATALINA_BASE CATALINA_OPTS JAVA_OPTS
